@extends("layouts.app")
@section("content")

<h4>{{$form->lib_form}}</h4>

<form action="{{ url('/answer') }}" method="POST">
        @csrf

        @foreach ($questions as $question)
        <h5>{{ $question->lib_question }}</h5>

        <ul class="question-list">
            @foreach ($answers as $answer)
                @if ($question->id_question == $answer->id_question)
                <li>
                    <label>
                        <input class="with-gap" type="radio" name="{{$answer->id_question}}" value="{{$answer->correct}}"/>
                        <span class="grey-text text-darken-3">{{ $answer->lib_answer}}</span>
                    </label>
                </li>
                @endif
            @endforeach
        </ul>


        @endforeach

        <input type="submit">

    </form>

	<p><a href="{{ url('/') }}" >Return to the questions list.</a></p>

@endsection
