@extends('layouts.app')

@section('content')

<h1>Results</h1>

<div>
    <br>
    <h4>Your Score is: {{$correct_responses}} / {{$renum}}</h4>
</div>
<p><a href="{{ url('/') }}" >Return to the questions list.</a></p>

@endsection
