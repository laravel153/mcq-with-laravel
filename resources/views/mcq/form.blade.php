@extends('layouts.app')

@section('content')

<h1>MCQs List</h1>

<div>
@foreach ($forms as $form)
<ul>
<li><a href="{{ url('questions/'.$form->id_form)}}">{{$form->lib_form}}</a></li>
</ul>
@endforeach

</div>

@endsection
