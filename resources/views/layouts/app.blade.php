<!DOCTYPE html>
<html>
<head>
    <title>MCQ</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
    <link rel="icon" href="{{ url('img/mcq-icon.png') }}">

</head>
<body>
<div class="container">
    <br>
    @yield('content')
</div>

</body>
</html>
