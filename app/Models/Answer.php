<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_answer',
        'id_question',
        'lib_answer',
        'correct'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
