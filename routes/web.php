<?php

use App\Http\Controllers\{FormController, QuestionController, AnswerController};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FormController::class, 'index']);
Route::get('/questions/{id}', [QuestionController::class, 'showQuestion']);
Route::post('/answer',[AnswerController::class,'result']);


/* Route::get('/questions/1', [FormController::class, 'showQuestion'])->name('showQuestion'); */
Route::resource('forms', FormController::class);


