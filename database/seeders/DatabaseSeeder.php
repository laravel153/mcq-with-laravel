<?php

namespace Database\Seeders;

use App\Models\{Form, Question, Answer};
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Form::insert([
            ['lib_form' => 'GNU'],
            ['lib_form' => 'Admin commands'],
        ]);

        Question::insert([
            ['id_form'=> 1, 'lib_question' => 'What does GNU stands for?'],
            ['id_form'=> 1, 'lib_question' => 'Who launched the GNU Project?'],
            ['id_form'=> 1, 'lib_question' => 'When the GNU Enterprise (GNUe) was started?'],
            ['id_form'=> 2, 'lib_question' => 'Which command is used to bring the background process to forground?'],
            ['id_form'=> 2, 'lib_question' => 'Which command could be used to determine the amount of free space left on a mounted drive?'],
            ['id_form'=> 2, 'lib_question' => 'Which command is used to remove a module from the kernel?'],
            ['id_form'=> 2, 'lib_question' => 'Which of the following commands return the exit status of last command?'],

        ]);

        Answer::insert([
            ['id_question'=>1,'lib_answer'=>'General Unix','correct'=>0],
            ['id_question'=>1,'lib_answer'=>'GNU\'s Not Unix','correct'=>1],
            ['id_question'=>1,'lib_answer'=>'General Noble Unix','correct'=>0],
            ['id_question'=>2,'lib_answer'=>'Richard Stallman','correct'=>1],
            ['id_question'=>2,'lib_answer'=>'Linus Torvalds','correct'=>0],
            ['id_question'=>2,'lib_answer'=>'James Gosling','correct'=>0],
            ['id_question'=>3,'lib_answer'=>'In 1996','correct'=>1],
            ['id_question'=>3,'lib_answer'=>'In 1999','correct'=>0],
            ['id_question'=>3,'lib_answer'=>'In 2002','correct'=>0],
            ['id_question'=>3,'lib_answer'=>'In 1991','correct'=>0],

            ['id_question'=>4,'lib_answer'=>'bg','correct'=>0],
            ['id_question'=>4,'lib_answer'=>'fourground','correct'=>0],
            ['id_question'=>4,'lib_answer'=>'background','correct'=>0],
            ['id_question'=>4,'lib_answer'=>'fg','correct'=>1],

            ['id_question'=>5,'lib_answer'=>'fs -al','correct'=>0],
            ['id_question'=>5,'lib_answer'=>'dir -a','correct'=>0],
            ['id_question'=>5,'lib_answer'=>'df -h','correct'=>1],
            ['id_question'=>5,'lib_answer'=>'dh -f','correct'=>0],

            ['id_question'=>6,'lib_answer'=>'Modprobe -a','correct'=>0],
            ['id_question'=>6,'lib_answer'=>'modprobe -b','correct'=>0],
            ['id_question'=>6,'lib_answer'=>'modprobe -d','correct'=>0],
            ['id_question'=>6,'lib_answer'=>'modprobe -r','correct'=>1],

            ['id_question'=>7,'lib_answer'=>'&!','correct'=>0],
            ['id_question'=>7,'lib_answer'=>'$!','correct'=>0],
            ['id_question'=>7,'lib_answer'=>'$?','correct'=>1],
            ['id_question'=>7,'lib_answer'=>'$$','correct'=>0],


        ]);


    }
}
