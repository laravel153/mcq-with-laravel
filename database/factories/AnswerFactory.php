<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Answer>
 */
class AnswerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lib_answer'=>$this->faker->sentence(1),
            'id_question'=>$this->faker->numberBetween(1,5),
            'correct'=>$this->faker->numberBetween(0,1),
        ];
    }
}
